import axios from 'axios';

const opt = { API_URL: process.env.API_URL, ACCESS_TOKEN: process.env.ACCESS_TOKEN };
const VERSION = require('../../../package.json').version;
const ALL_HTTP = {};
const HTTPV = baseURL => {
  if (!baseURL) baseURL = opt.API_URL;
  if (ALL_HTTP[baseURL]) return ALL_HTTP[baseURL];
  const HTTP = (ALL_HTTP[baseURL] = axios.create({
    baseURL: baseURL,
    headers: {
      authorization: localStorage.getItem('access_token') || opt.ACCESS_TOKEN
      // version: VERSION
    }
  }));
  HTTP.interceptors.request.use(
    function(config) {
      return config;
    },
    function(error) {
      // Do something with request error
      return Promise.reject(error);
    }
  );

  HTTP.interceptors.response.use(
    response => {
      if (response.data && response.data.$$notification && response.data.$$notification.success) {
        console.error(response.data.$$notification.success);
      }
      return response;
    },
    error => {
      // Version conflict
      console.log(`Version: ${VERSION} ${window.location.pathname}`);
      if (error.response.status === 409) {
        console.log(`Version conflict v${VERSION}`);
      }
      // Do something with response error
      if (error.response.status === 401) {
      }

      if (error.response.data && error.response.data.$$notification && error.response.data.$$notification.error) {
        console.error(error.response.data.$$notification.success);
      }

      return Promise.reject(error.response);
    }
  );
  return ALL_HTTP[baseURL];
};
const HTTP = HTTPV();
export { HTTP as default, HTTPV };
