import Vue from 'vue';
import App from './App.vue';
import { BootstrapVue } from 'bootstrap-vue';
import './assets/css/flex.css';

// import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import TopTable from '../../src/index.js';
import ValidatedInput from '@mtntop/validated-input';
import HTTP from './utils/http-common';

const opt = { API_URL: process.env.API_URL };
Vue.use(BootstrapVue);
Vue.use(TopTable);
Vue.use(ValidatedInput);
Vue.mixin({
  data() {
    return {
      ...opt,
      $route: {} //Router is required for saving the filters in cache
    };
  },
  computed: {
    http() {
      return HTTP;
    }
  }
});

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  render: h => h(App)
});
