export const encodeOpts = opts => {
  let str = '';
  for (let key in opts) {
    if (str !== '') {
      str += '&';
    }
    str += key + '=' + encodeURIComponent(JSON.stringify(opts[key]));
  }
  return str;
};
export const startCase = function(value) {
  let wordsArray = value.replace(/([A-Z]+)/g, " $1").trim().split(/\s+/);
  let capsArray = [];

  wordsArray.forEach(word => {
    capsArray.push(word[0].toUpperCase() + word.slice(1));
  });

  return capsArray.join(' ');
};
export const each = (obj, callback) => {
  Array.isArray(obj) ? obj.forEach(e => callback(obj[e], e)) : Object.keys(obj).forEach(e => callback(obj[e], e));
};
