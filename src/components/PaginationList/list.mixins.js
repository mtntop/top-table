import { each, encodeOpts } from '../../utils/index';
//TODO: should use this mixin on top-table as well
export default {
  props: {
    prefix: {
      type: String,
      default: '',
    },
    url: {
      type: String,
      required: true,
    },
    method: {
      type: String,
      default: 'get',
    },

    tabFilters: {
      type: Array,
      default() {
        return [];
      },
    },

    searchColumns: {
      type: Array,
      default: () => [],
    },
    selectedFilter: {
      type: Object,
      default() {
        return {};
      },
    },
    hasTopBar: {
      type: Boolean,
      default: false,
    },
    requestFunction: { type: Function },
    requestFiltersSeparately: { type: Function },
    shouldSetQuery: { type: Boolean, default: false },
    copyFilterInRoot: { type: Boolean, default: true },
    keepInGlobalStore: { type: Boolean, default: true },
    includeAllFilters: { type: Boolean, default: true },
    forceResetFilter: { type: Boolean, default: false },
    hasClearFilters: { type: Boolean, default: false },
    noSaveFilter: { type: Boolean },
  },
  data() {
    return {
      filter: this.setDefaultFilter(),
      selected: [],

      canLoad: false,
      filteredChecked: false,
      loading: false,
      lastFilter: null,
      error: null,
      isFilterReseted: false,
      noClearFilters: false,
      loadedFilter: false,
      loadingFilters: false,
    };
  },
  watch: {
    filter: {
      handler(filter) {
        this.refresh();
      },
      deep: true,
    },
    selectedFilter: {
      handler(selected) {
        for (const prop in this.selectedFilter) {
          this.filter[prop] = this.selectedFilter[prop];
        }
      },
      deep: true,
    },
    tabFilters: {
      handler(newVal, oldVal) {
        for (let i = 0; i < newVal.length; i++) {
          if (oldVal[i].criteria.length !== newVal[i].criteria.length) {
            return this.checkFilter();
          }
        }
      },
      deep: true,
    },
  },
  computed: {
    filterId() {
      return `${this.$options.name}-mpower-list-filters-${this.VERSION_TABLE}-${
        this.prefix || (this.$route && this.$route.path)
      }-${this.url}`;
    },
  },
  methods: {
    requestParams(data) {
      const that = this;
      const a = {};
      each(that.filter, (value, key) => {
        a[key] = value;
        if (!that.includeAllFilters && (!value || !value.length)) delete a[key]; //if no need to include filters will remove keys
      });

      that.tabFilters.forEach((i) => {
        if (i.keyFilter && Array.isArray(a[i.val]) && a[i.val].filter((e) => e).length) {
          a[i.val] = a[i.val].map((el) => el[i.keyFilter] || el);
        }
      });
      let params = {};
      if (that.copyFilterInRoot) {
        params = {
          ...Object.assign({ filter: a }, data),
          ...a,
          ...data,
        };
      } else {
        params = Object.assign({ filter: a }, data);
      }
      params.limit = Number(params.limit);
      that.lastFilter = params;

      if (this.requestFunction) this.requestFunction(params);
      Object.keys(params).forEach((key) => {
        if (Array.isArray(params[key]) && params[key].length === 0) {
          params[key] = params.filter[key] = null; //no need to send empty array
          delete params[key];
          delete params.filter[key];
        }
      });
      this.tabFilters.forEach((i) => {
        if (params[i.val]) params.filter[i.val] = params[i.val];
      });

      Object.keys(params.filter).forEach((el) => {
        if (Array.isArray(params.filter[el]) && params.filter[el][0] && params.filter[el][0].val)
          params.filter[el] = params.filter[el].map((v) => ({ val: v.val }));
      });
      if (this.keepInGlobalStore) {
        let _filter = JSON.parse(JSON.stringify(params)); //keep in global store
        localStorage.setItem(this.filterId, JSON.stringify(_filter));
        (this.$parent && this.$parent.setQuery ? this.$parent : this).setQuery(_filter);
      }
      return params;
    },
    async checkFilters(params) {
      try {
        if (!this.requestFiltersSeparately || this.loadedFilter || this.loadingFilters || !this.canLoad) return;
        this.loadingFilters = true;

        const res = (
          await (this.http || this.$axios)[this.method](this.url, {
            params: {
              ...this.requestFiltersSeparately(params),
              onlyFilters: true,
            },
            paramsSerializer: encodeOpts,
          })
        ).data;
        this.$emit('onLoadFilters', res);
        this.loadedFilter = true;
      } catch (e) {
        console.log(e);
      } finally {
        this.loadingFilters = false;
      }
    },
    checkFilter() {
      if (this.filteredChecked) return;
      this.filteredChecked = true;
      this.canLoad = false;
      if (this.forceResetFilter && !this.isFilterReseted) {
        this.isFilterReseted = true;
        localStorage.removeItem(this.filterId);
      }
      let globalFilter = !this.noSaveFilter && localStorage.getItem(this.filterId);
      if (globalFilter) {
        this.filter = {
          ...JSON.parse(JSON.stringify(this.filter)),
          ...JSON.parse(globalFilter),
        };
        localStorage.setItem(this.filterId, JSON.stringify(this.filter));
        this.resetFilter();
      } else if (this.$route && this.$route.query.filter) {
        try {
          const filter = JSON.parse(decodeURIComponent(this.$route.query.filter));
          this.filter = {
            ...JSON.parse(JSON.stringify(this.filter)),
            ...filter,
          };
          this.resetFilter();
        } catch (e) {
          console.log(e);
        }
      } else {
        this.canLoad = true;
        this.filteredChecked = false;
        if (!this.$table) this.refresh();
        return true;
      }
      setTimeout(() => {
        this.filteredChecked = false;
      }, 1000);
    },
    clearFilters() {
      this.noClearFilters = true;
      if (this.filterId) {
        setTimeout(() => {
          localStorage.removeItem(this.filterId);
        });
      } else {
        for (let i = 0, list = Object.entries(localStorage); i < list.length; i++) {
          if (list[i][0].match(`${this.$options.name}-mpower-table-filters-${this.VERSION_TABLE}-`)) {
            setTimeout(() => {
              localStorage.removeItem(list[i][0]);
            });
          }
        }
      }

      setTimeout(async () => {
        setTimeout(() => {
          this.onClearFilter();
          this.$emit('clearFilters');
          this.filter = null;
          Object.keys(this.selectedFilter).forEach((el) => delete this.selectedFilter[el]);
          this.setDefaultFilter();
          this.noClearFilters = false;
        }, 100);
      });
    },
    setDefaultFilter() {
      const filter = {};

      this.tabFilters.forEach((i) => {
        if (i.selected && i.keyFilter && i.criteria.length) {
          i.selected = i.criteria.filter((el) => i.selected.indexOf(el[i.keyFilter]) > -1);
        }
        filter[i.val] = i.selected || [];
      });

      for (const prop in this.selectedFilter) {
        filter[prop] = this.selectedFilter[prop];
      }
      if (!this.filter) {
        this.filter = filter;
      } else {
        Object.assign(this.filter, filter);
      }
      return filter;
    },

    setQuery(query) {
      if (this.shouldSetQuery) {
        this.$router &&
          this.$router.replace({
            ...this.$router.currentRoute,
            query: { filter: encodeURIComponent(JSON.stringify(query)) },
          });
      }
    },
    resetFilter() {
      const self = this;
      this.tabFilters.forEach((i) => {
        const selected = self.filter[i.val];
        if (selected && i.keyFilter && i.criteria.length) {
          for (let j = 0; j < selected.length; j++) {
            if (typeof selected[j] !== 'object') {
              selected[j] = i.criteria.filter((el) => selected[j] === el[i.keyFilter])[0] || selected[j];
            }
          }
        }
        this.filter[i.val] = selected || [];
      });
      this.setTableFilter();
      this.canLoad = true;
    },
    onClearFilter() {}, //TODO:to override
    refresh() {}, //TODO:to override
    setTableFilter() {}, //TODO:to override
  },
  mounted() {
    this.checkFilter();
  },
};
