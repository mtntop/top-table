import { each, encodeOpts } from './utils';

export default {
  props: {
    searchColumns: {
      type: Array,
      default: () => [],
    },
    method: { type: String, default: 'get' },
    requestFiltersSeparately: { type: Function },
    keepInGlobalStore: { type: Boolean, default: true },
    copyFilterInRoot: { type: Boolean, default: true },
    includeAllFilters: { type: Boolean, default: true },
    shouldSetQuery: { type: Boolean, default: false },
    requestFunction: { type: Function },
    forceResetFilter: { type: Boolean, default: false },
    tabFilters: {
      type: Array,
      default() {
        return [];
      },
    },
    prefix: {
      type: String,
      default: '',
    },
    selectedFilter: {
      type: Object,
      default() {
        return {};
      },
    },
    hideFilters: {
      type: Boolean,
      default: false,
    },
    url: {
      type: String,
      required: true,
    },
  },
  watch: {
    filter: {
      handler(filter) {
        if (this.$refs.table && this.refresh) this.refresh();
      },
      deep: true,
    },
    selectedFilter: {
      handler(selected) {
        for (const prop in this.selectedFilter) {
          this.filter[prop] = this.selectedFilter[prop];
        }
      },
      deep: true,
    },
    tabFilters: {
      handler(newVal, oldVal) {
        for (let i = 0; i < newVal.length; i++) {
          if ((oldVal[i].criteria || []).length !== (newVal[i].criteria || []).length) {
            return this.checkFilter();
          }
        }
      },
      deep: true,
    },
  },
  data() {
    const filter = this.setDefaultFilter();
    return {
      uniquePrefix: Date.now(),
      filter,
      noClearFilters: false,
      isFilterReseted: false,
      lastFilter: null,
      loadingFilters: false,
      loadedFilter: false,
    };
  },
  computed: {
    filterId() {
      return `mpower-table-filters-${this.VERSION_TABLE}-${
        this.prefix || (this.$route && this.$route.path) || location.pathname
      }-${this.url}`;
    },
  },
  methods: {
    clearFilters() {
      this.noClearFilters = true;
      if (this.filterId) {
        setTimeout(() => {
          localStorage.removeItem(this.filterId);
        });
      } else {
        for (let i = 0, list = Object.entries(localStorage); i < list.length; i++) {
          if (list[i][0].match(`mpower-table-filters-${this.VERSION_TABLE}`)) {
            setTimeout(() => {
              localStorage.removeItem(list[i][0]);
            });
          }
        }
      }

      setTimeout(async () => {
        setTimeout(() => {
          this.$emit('clearFilters');
          this.filter = null;
          Object.keys(this.selectedFilter).forEach((el) => delete this.selectedFilter[el]);
          this.setDefaultFilter();
          this.noClearFilters = false;
        }, 100);
      });
    },
    async tableFilter(parent) {
      const that = parent || this;
      const data = {};
      const a = {};
      each(that.filter, (value, key) => {
        a[key] = value;
        if (!that.includeAllFilters && (!value || !value.length)) delete a[key]; //if no need to include filters will remove keys
      }); //

      that.tabFilters.forEach((i) => {
        if (i.keyFilter && Array.isArray(a[i.val]) && a[i.val].filter((e) => e).length) {
          a[i.val] = a[i.val].map((el) => el[i.keyFilter] || el);
        }
        if (i.date && a[i.val] && !a[i.val].startDate && !a[i.val].endDate) {
          delete a[i.val];
        }
      });
      let params = {};
      const filter = JSON.parse(JSON.stringify(a));
      if (that.copyFilterInRoot) {
        params = {
          ...Object.assign({ filter }, data),
          ...filter,
          ...data,
        };
      } else {
        params = Object.assign({ filter }, data);
      }
      params.limit = Number(params.limit);
      that.lastFilter = params;

      if (that.requestFunction) that.requestFunction(params);
      Object.keys(params).forEach((key) => {
        if (Array.isArray(params[key]) && params[key].length === 0) {
          params[key] = params.filter[key] = null; //no need to send empty array
          delete params[key];
          delete params.filter[key];
        }
      });
      that.tabFilters.forEach((i) => {
        if (params[i.val]) params.filter[i.val] = params[i.val];
      });

      Object.keys(params.filter).forEach((el) => {
        if (Array.isArray(params.filter[el]) && params.filter[el][0] && params.filter[el][0].val)
          params.filter[el] = params.filter[el].map((v) => ({ val: v.val }));
      });
      return params;
    },
    async checkFilters(params) {
      try {
        if (!this.requestFiltersSeparately || this.loadedFilter || this.loadingFilters) return;
        this.loadingFilters = true;

        const res = (
          await (this.http || this.$axios)[this.method](this.url, {
            params: {
              ...this.requestFiltersSeparately(params),
              onlyFilters: true,
            },
            paramsSerializer: encodeOpts,
          })
        ).data;
        this.$emit('onLoadFilters', res);
        this.loadedFilter = true;
      } catch (e) {
      } finally {
        this.loadingFilters = false;
      }
    },
    setDefaultFilter() {
      const filter = {};

      this.tabFilters.forEach((i) => {
        if (i.selected && i.keyFilter && i.criteria.length) {
          i.selected = i.criteria.filter((el) => i.selected.indexOf(el[i.keyFilter]) > -1);
        }
        if (i.text) {
          filter[i.val] = i.value = this.selectedFilter[i.val] || '';
        } else {
          filter[i.val] = i.selected || (i.date ? {} : []);
        }
      });

      for (const prop in this.selectedFilter) {
        filter[prop] = this.selectedFilter[prop];
      }
      if (!this.filter) {
        this.filter = filter;
      } else {
        Object.assign(this.filter, filter);
      }
      return filter;
    },
    setQuery(query) {
      if (this.shouldSetQuery) {
        this.$router &&
          this.$router.replace({
            ...this.$router.currentRoute,
            query: { filter: encodeURIComponent(JSON.stringify(query)) },
          });
      }
    },
    resetFilter() {
      const self = this;
      this.tabFilters.forEach((i) => {
        const selected = self.filter[i.val];
        if (selected && i.keyFilter && i.criteria.length) {
          for (let j = 0; j < selected.length; j++) {
            if (typeof selected[j] !== 'object') {
              selected[j] = i.criteria.filter((el) => selected[j] === el[i.keyFilter])[0] || selected[j];
            }
          }
        }
        this.filter[i.val] = selected || [];
      });
      this.setTableFilter();
    },

    checkFilter() {
      if (this.forceResetFilter && !this.isFilterReseted) {
        this.isFilterReseted = true;
        localStorage.removeItem(this.filterId);
      }
      let globalFilter = localStorage.getItem(this.filterId);
      if (globalFilter) {
        this.filter = {
          ...JSON.parse(JSON.stringify(this.filter)),
          ...JSON.parse(globalFilter),
        };
        localStorage.setItem(this.filterId, JSON.stringify(this.filter));
        this.resetFilter();
      } else if (this.$route && this.$route.query.filter) {
        try {
          const filter = JSON.parse(decodeURIComponent(this.$route.query.filter));
          this.filter = {
            ...JSON.parse(JSON.stringify(this.filter)),
            ...filter,
          };
          this.resetFilter();
        } catch (e) {
          console.log(e);
        }
      } else {
        this.canLoad = true;
        return true;
      }
    },
  },
};
