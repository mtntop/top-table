import Vue from 'vue';
import PaginationList from './components/PaginationList/index';
import TopTable from './components/TopTable.vue';
const VERSION = require('../package.json').version;
import { ClientTable, ServerTable } from 'vue-tables-2';

export default {
  install(Vue, options) {
    Vue.use(
      ClientTable,
      {
        skin: 'table table-striped',
        sortIcon: {
          base: 'fal',
          up: 'fa-sort-amount-up',
          down: 'fa-sort-amount-down'
        },
        perPage: 10
      },
      false,
      'bootstrap4'
    );
    Vue.use(
      ServerTable,
      {
        skin: 'table table-striped',
        sortIcon: {
          base: 'fal',
          up: 'fa-sort-amount-up',
          down: 'fa-sort-amount-down'
        },
        perPage: 10
      },
      false,
      'bootstrap4'
    );

    Vue.mixin({
      data() {
        return {
          VERSION_TABLE: VERSION
        };
      }
    });
    Vue.component('TopTable', TopTable);
    Vue.component('PaginationList', PaginationList);
    Vue.component('customIcon', {
      template: `<span class="far fa-angle-down fa-xs text-muted"></span>`
    });

    console.log('MPowerTable %s', VERSION);
  }
};
