# Top Table

## Usage example

In `main.js`:

```javascript
import TopTable from '@mtntop/top-table';

Vue.use(TopTable);
```

#Example without filters:

```vue
<top-table
  url="/orders"
  :columns="['orderId', 'orderStatus', 'orderTotal', 'firstName', 'lastName', 'company', 'email', 'createdAt']"
  :tab-filters="[{ val: 'assigned', criteria: ['Yes', 'No'] }]"
>
  <template v-slot:orderId="props">
    <router-link :to="`/orders/${props.row._id}`" class="btn btn-secondary">{{ props.row.orderId }}</router-link>
  </template>
</top-table>
```

#Example for PaginationList:

```vue
 <PaginationList
          :requestFiltersSeparately="requestFiltersSeparately"
          @onLoadFilters="onLoadFilters"
          hasClearFilters
          :url="`/products/${record.LineItem.localProduct._id}/media-images`"
          :requestFunction="onPreRequest"
          :tabFilters="tabFilters"
        >
          <template v-slot:record="props">
            <div class="col-md-3 col-lg-2 col-sm-6 col-xs-4 mb-3">
              <img :src="props.item.urlSmall" class="img-fluid img-thumbnail" />
            </div>
          </template>
        </PaginationLoading>
```

Tab filters options:

- val - what will be sent to server
- name - how the tab should be called
- criteria - Array of Criteria

Criteria:

Should be String or Object. Object params:

- val - what will be sent to server
- label - checkbox label

## Push Development Changes:

1. Run `npm version patch` - this will change version, create git tags, push git tags
1. Run `npm run build` - build the app
1. Run `npm publish --access public` - publish last build to remote repo, before publish make sure you have dupgrade version and build

### Development :

1. `npm run watch:server` - start front end dev
